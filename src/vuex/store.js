import { createStore } from "vuex";

export default createStore({
  state: {
    user: {},
    searchArray: [],
    isSearch: false,
    path: [{name: '', path: '', params: ''}, {name: '', path: '', params: ''}, {name: '', path: '', params: ''}, {name: '', path: '', params: ''},]
  },
  mutations: {
    SEARCH: (state, el) => {
      state.searchArray = el
      state.isSearch = true
      state.path[0] = {name: '', path: '', params: ''}
      state.path[1] = {name: '', path: '', params: ''}
      state.path[2] = {name: '', path: '', params: ''}
      state.path[3] = {name: '', path: '', params: ''}
    },
    SEARCH_CLEAR: (state) => {
      state.searchArray = []
      state.isSearch = false
    },
    SET_STATE_SEARCH: (state) => {
      state.isSearch = !state.isSearch
    },
    SET_PATH: (state, el) => {
      // if (el.index === 0) state.path[0] = {name: '', path: '', params: ''}
      if (el.index === 0) {
        state.path[1] = {name: '', path: '', params: ''}
        state.path[2] = {name: '', path: '', params: ''}
        state.path[3] = {name: '', path: '', params: ''}
      }
      if (el.index === 2) {
        state.path[2] = {name: '', path: '', params: ''}
        state.path[3] = {name: '', path: '', params: ''}
      }
      if (el.index === 3) {
        state.path[3] = {name: '', path: '', params: ''}
      }
      state.path[el.index] = {name: el.name, path: el.path, params: el.params}
    }
  },
  getters: {
    searchArr (state) {
      return state.searchArray
    },
    getPath (state) {
      return state.searchArray
    },
    getIsSearch (state) {
      return state.isSearch
    },
  }
});
// export const store = reactive({
//   user: {},
//   profile: {},
//   shop: [],
//   basket: []
// })
// export let basket = reactive([])
