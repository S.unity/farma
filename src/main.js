import { createApp } from 'vue'
import App from './App.vue'
import "./assets/style.scss"
import router from './router';
import clickOutside from './utils/click-outside';
import store from './vuex/store';
import { Quasar } from 'quasar'

// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css'
// Import Quasar css
import 'quasar/src/css/index.sass'
import './assets/style.scss'
// запрет на вход не авторизованному пользователю
// router.beforeEach(async (to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     /*
//     этот путь требует авторизации, проверяем залогинен ли
//     пользователь, и если нет, запрещаем доступ к странице
//      */
//     if (store.user && store.user.id) {
//       next()
//     } else {
//       console.log('Пользователь не зарегестрирован')
//       next({
//         path: '/404'
//       })
//     }
//   } else {
//     next() // всегда так или иначе нужно вызвать next()!
//   }
// })

createApp(App)
  .use(store)
  .use(Quasar, {
    plugins: {}, // import Quasar plugins and add here
  })
  .use(router)
  .directive('click-outside', clickOutside)
  .mount('#app')
