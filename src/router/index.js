import { createRouter, createMemoryHistory, createWebHistory } from 'vue-router';

const isServer = typeof window === 'undefined';
const history = isServer ? createMemoryHistory() : createWebHistory();
const routes = [
    {
        path: '/',
        name: 'Index',
        component: ()=> import('../views/Index.vue'),
    },
    {
        path: '/category/:id',
        name: 'Category',
        component: ()=> import('../views/Category.vue'),
        props: true
    },
    {
        path: '/LaboratoryResearch/:id',
        name: 'LaboratoryResearch',
        component: ()=> import('../views/LaboratoryResearch.vue'),
        props: true
    },
    {
        path: '/ResearchOption/:id',
        name: 'ResearchOption',
        component: ()=> import('../views/ResearchOption.vue'),
        props: true
    },
];
const router = createRouter({
    history,
    routes,
});

export default router;
